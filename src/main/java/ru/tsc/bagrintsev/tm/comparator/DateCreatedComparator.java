package ru.tsc.bagrintsev.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.model.IHasDateCreated;

import java.util.Comparator;

public enum DateCreatedComparator implements Comparator<IHasDateCreated> {

    INSTANCE;

    @Override
    public int compare(
            @Nullable final IHasDateCreated o1,
            @Nullable final IHasDateCreated o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getDateCreated().compareTo(o2.getDateCreated());
    }
}
