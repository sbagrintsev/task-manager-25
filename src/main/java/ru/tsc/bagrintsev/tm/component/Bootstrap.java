package ru.tsc.bagrintsev.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.bagrintsev.tm.api.repository.ICommandRepository;
import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.repository.IUserRepository;
import ru.tsc.bagrintsev.tm.api.sevice.*;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.repository.CommandRepository;
import ru.tsc.bagrintsev.tm.repository.ProjectRepository;
import ru.tsc.bagrintsev.tm.repository.TaskRepository;
import ru.tsc.bagrintsev.tm.repository.UserRepository;
import ru.tsc.bagrintsev.tm.service.*;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.security.GeneralSecurityException;
import java.util.Set;

@Getter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.bagrintsev.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        classes.forEach(this::registry);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        @NotNull final AbstractCommand command = clazz.getDeclaredConstructor().newInstance();
        registry(command);
    }

    public void run(@Nullable final String[] args) throws IOException {
        initLogger();
        try {
            processOnStart(args);
        } catch (AbstractException | GeneralSecurityException e) {
            loggerService.error(e);
        }
        try {
            initUsers();
        } catch (GeneralSecurityException | AbstractException e) {
            System.err.println("User initialization error...");
            loggerService.error(e);
        }
        try {
            initData();
        } catch (AbstractException e) {
            System.err.println("Data initialization error...");
            loggerService.error(e);
        }
        while (true) {
            try {
                System.out.println();
                System.out.println("Enter Command:");
                System.out.print(">> ");
                @NotNull final String command = TerminalUtil.nextLine();
                processOnTheGo(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initData() throws AbstractException {
        taskService.create("first task", "task simple description");
        taskService.create("second task", "task simple description");
        taskService.create("third task", "task simple description");
        taskService.create("fourth task", "task simple description");

        projectService.create("first project", "project simple description");
        projectService.create("second project", "project simple description");
        projectService.create("third project", "project simple description");
        projectService.create("fourth project", "project simple description");
    }

    private void initLogger() {
        loggerService.info("*** Welcome to Task Manager ***");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("*** Task Manager is shutting down ***");
            }
        });
    }

    private void initUsers() throws GeneralSecurityException, AbstractException {
        userService.create("test", "test").setEmail("test@test.ru");
        userService.create("admin", "admin").setRole(Role.ADMIN);
    }

    private void processOnStart(@Nullable final String arg) throws AbstractException, IOException, GeneralSecurityException {
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByShort(arg);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processOnStart(@Nullable final String[] args) throws AbstractException, IOException, GeneralSecurityException {
        if (args == null || args.length == 0) return;
        @Nullable final String arg = args[0];
        processOnStart(arg);
        commandService.getCommandByName("exit").execute();
    }

    private void processOnTheGo(@Nullable final String command) throws IOException, AbstractException, GeneralSecurityException {
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

}
