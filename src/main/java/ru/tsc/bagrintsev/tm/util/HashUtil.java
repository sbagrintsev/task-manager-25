package ru.tsc.bagrintsev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

public interface HashUtil {

    @NotNull
    static byte[] generateSalt() {
        @NotNull SecureRandom random = new SecureRandom();
        @NotNull byte[] salt = new byte[16];
        random.nextBytes(salt);
        return salt;
    }

    @NotNull
    static String generateHash(
            @NotNull String password,
            @Nullable byte[] salt,
            @NotNull final Integer iterations,
            @NotNull final Integer keyLength) throws GeneralSecurityException {
        @NotNull final KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, keyLength);
        @NotNull final SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        @NotNull final byte[] hash = factory.generateSecret(spec).getEncoded();
        return new String(hash, StandardCharsets.UTF_8);
    }

}
