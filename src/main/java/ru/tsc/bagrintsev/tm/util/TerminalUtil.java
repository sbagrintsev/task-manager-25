package ru.tsc.bagrintsev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.system.WrongDateFormatException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public interface TerminalUtil {

    @NotNull
    BufferedReader READER = new BufferedReader(new InputStreamReader(System.in));

    @NotNull
    static String nextLine() throws IOException {
        String s = READER.readLine();
        if (s == null) s = "";
        return s;
    }

    @NotNull
    static Integer nextNumber() throws IOException {
        return Integer.parseInt(READER.readLine());
    }

    @NotNull
    static Date nextDate() throws IOException, WrongDateFormatException {
        return DateUtil.toDate(READER.readLine());
    }

}
