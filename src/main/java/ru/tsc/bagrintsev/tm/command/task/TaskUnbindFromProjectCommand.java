package ru.tsc.bagrintsev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {
    @Override
    public void execute() throws IOException, AbstractException {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.print("ENTER TASK ID: ");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-unbind-from-project";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Unbind task from project.";
    }
}
