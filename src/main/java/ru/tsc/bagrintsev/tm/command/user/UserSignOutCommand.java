package ru.tsc.bagrintsev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

import java.io.IOException;

public final class UserSignOutCommand extends AbstractUserCommand {

    @Override
    public void execute() throws IOException, AbstractException {
        showOperationInfo();
        getAuthService().signOut();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getName() {
        return "user-sign-out";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sign user out.";
    }

}
