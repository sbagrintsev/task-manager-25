package ru.tsc.bagrintsev.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.model.ICommand;
import ru.tsc.bagrintsev.tm.api.sevice.IAuthService;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.system.ServiceLocatorNotInitializedException;

import java.io.IOException;
import java.security.GeneralSecurityException;

public abstract class AbstractCommand implements ICommand {

    @Nullable
    protected IServiceLocator serviceLocator;

    public abstract void execute() throws IOException, AbstractException, GeneralSecurityException;

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getShortName();

    @NotNull
    public abstract String getDescription();

    @NotNull
    public abstract Role[] getRoles();

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void showOperationInfo() {
        System.out.printf("[%s]%n", getName());
    }

    protected void showParameterInfo(@NotNull final EntityField parameter) {
        System.out.printf("Enter %s: %n", parameter.getDisplayName());
    }

    @NotNull
    public IAuthService getAuthService() throws AbstractException {
        if (serviceLocator == null) throw new ServiceLocatorNotInitializedException();
        return serviceLocator.getAuthService();
    }

    @Nullable
    public String getUserId() throws AbstractException {
        return getAuthService().getCurrentUserId();
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull StringBuilder result = new StringBuilder();
        result.append("[");
        result.append(String.format("%-35s", getName()));
        result.append(" | ");
        result.append(String.format("%-25s", getShortName()));
        result.append("]");
        if (!getDescription().isEmpty()) {
            while (result.length() < 75) {
                result.append(" ");
            }
            result.append(getDescription());
        }
        return result.toString();
    }

}
