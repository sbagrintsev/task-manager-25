package ru.tsc.bagrintsev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @Override
    public void execute() throws IOException, AbstractException {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final String userId = getUserService().findByLogin(login).getId();
        showParameterInfo(EntityField.FIRST_NAME);
        @NotNull final String firstName = TerminalUtil.nextLine();
        showParameterInfo(EntityField.MIDDLE_NAME);
        @NotNull final String middleName = TerminalUtil.nextLine();
        showParameterInfo(EntityField.LAST_NAME);
        @NotNull final String lastName = TerminalUtil.nextLine();
        try {
            getUserService().updateUser(userId, firstName, lastName, middleName);
        } catch (IdIsEmptyException e) {
            throw new UserNotFoundException();
        }
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getName() {
        return "user-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update user profile.";
    }

}
