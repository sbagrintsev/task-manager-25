package ru.tsc.bagrintsev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

public class AboutCommand extends AbstractSystemCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[Author]");
        System.out.printf("Name: %s%n", getPropertyService().getAuthorName());
        System.out.printf("E-mail: %s%n", getPropertyService().getAuthorEmail());
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print about author.";
    }

}
