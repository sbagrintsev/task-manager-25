package ru.tsc.bagrintsev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

public class VersionCommand extends AbstractSystemCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.printf("task-manager version: %s%n", getPropertyService().getApplicationVersion());
    }

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print version.";
    }

}
