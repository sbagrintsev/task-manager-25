package ru.tsc.bagrintsev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

import java.util.Collection;

public class CommandsCommand extends AbstractSystemCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[Interaction commands]");
        @NotNull Collection<AbstractCommand> repository = getCommandService().getAvailableCommands();
        repository.stream()
                .filter(c -> !c.getName().isEmpty())
                .forEach(c -> System.out.printf("%-35s%s\n", c.getName(), c.getDescription()));
    }

    @NotNull
    @Override
    public String getName() {
        return "commands";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print application interaction commands.";
    }

}
