package ru.tsc.bagrintsev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.security.GeneralSecurityException;

public final class UserUnlockCommand extends AbstractUserCommand {
    @Override
    public void execute() throws IOException, AbstractException, GeneralSecurityException {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        @NotNull final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getName() {
        return "user-unlock";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Admin functional: unlock user.";
    }
}
