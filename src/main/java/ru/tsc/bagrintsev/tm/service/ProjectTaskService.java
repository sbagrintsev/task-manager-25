package ru.tsc.bagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectTaskService;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    private void checkIfEntityOK(
            @Nullable final String projectId,
            @Nullable final String taskId) throws AbstractException {
        check(EntityField.PROJECT_ID, projectId);
        check(EntityField.TASK_ID, taskId);
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(taskId)) throw new TaskNotFoundException();
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) throws AbstractException {
        check(EntityField.USER_ID, userId);
        checkIfEntityOK(projectId, taskId);
        taskRepository.setProjectId(userId, taskId, projectId);
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) throws AbstractException {
        check(EntityField.USER_ID, userId);
        checkIfEntityOK(projectId, taskId);
        taskRepository.setProjectId(userId, taskId, null);
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.PROJECT_ID, projectId);
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        taskRepository.removeAll(tasks);
        projectRepository.removeById(userId, projectId);
    }

}
