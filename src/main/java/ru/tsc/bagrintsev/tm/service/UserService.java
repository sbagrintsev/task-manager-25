package ru.tsc.bagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.IProjectRepository;
import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.repository.IUserRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.api.sevice.IUserService;
import ru.tsc.bagrintsev.tm.enumerated.Entity;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.user.EmailAlreadyExistsException;
import ru.tsc.bagrintsev.tm.exception.user.LoginAlreadyExistsException;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.bagrintsev.tm.util.HashUtil;

import java.security.GeneralSecurityException;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IUserRepository repository,
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository,
            @NotNull final IPropertyService propertyService) {
        super(repository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) throws AbstractException {
        check(EntityField.LOGIN, login);
        return repository.findByLogin(login);
    }

    @NotNull
    @Override
    public User findByEmail(@Nullable final String email) throws AbstractException {
        check(EntityField.EMAIL, email);
        return repository.findByEmail(email);
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        try {
            findByLogin(login);
        } catch (AbstractException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        try {
            findByEmail(email);
        } catch (AbstractException e) {
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) throws AbstractException {
        check(EntityField.LOGIN, login);
        @NotNull final User user = repository.removeByLogin(login);
        @NotNull final String userId = user.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password) throws GeneralSecurityException, AbstractException {
        check(EntityField.LOGIN, login);
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        check(EntityField.PASSWORD, password);
        @NotNull final Integer iterations = propertyService.getPasswordHashIterations();
        @NotNull final Integer keyLength = propertyService.getPasswordHashKeyLength();
        @NotNull final byte[] salt = HashUtil.generateSalt();
        @NotNull final String passwordHash = HashUtil.generateHash(password, salt, iterations, keyLength);
        return repository.create(login, passwordHash, salt);
    }

    @NotNull
    @Override
    public User setParameter(
            @Nullable final User user,
            @Nullable final EntityField paramName,
            @Nullable final String paramValue) throws AbstractException {
        check(Entity.USER, user);
        if (EntityField.EMAIL.equals(paramName) && isEmailExists(paramValue))
            throw new EmailAlreadyExistsException(paramValue);
        return repository.setParameter(user, paramName, paramValue);
    }

    @NotNull
    @Override
    public User setRole(
            @Nullable final User user,
            @Nullable final Role role) throws AbstractException {
        check(Entity.USER, user);
        check(Entity.ROLE, role);
        return repository.setRole(user, role);
    }

    @NotNull
    @Override
    public User setPassword(
            @Nullable final String userId,
            @Nullable final String password) throws AbstractException, GeneralSecurityException {
        check(EntityField.ID, userId);
        check(EntityField.PASSWORD, password);
        @NotNull final User user = findOneById(userId);
        @NotNull final Integer iterations = propertyService.getPasswordHashIterations();
        @NotNull final Integer keyLength = propertyService.getPasswordHashKeyLength();
        @NotNull final byte[] salt = HashUtil.generateSalt();
        @NotNull final String passwordHash = HashUtil.generateHash(password, salt, iterations, keyLength);
        repository.setUserPassword(user, passwordHash, salt);
        return user;
    }

    @NotNull
    @Override
    public User updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName) throws AbstractException {
        check(EntityField.ID, userId);
        @NotNull final User user = findOneById(userId);
        setParameter(user, EntityField.FIRST_NAME, firstName);
        setParameter(user, EntityField.MIDDLE_NAME, middleName);
        setParameter(user, EntityField.LAST_NAME, lastName);
        return user;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws AbstractException {
        check(EntityField.LOGIN, login);
        @NotNull final User user = repository.findByLogin(login);
        repository.setParameter(user, EntityField.LOCKED, "true");
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws AbstractException {
        check(EntityField.LOGIN, login);
        @NotNull final User user = repository.findByLogin(login);
        repository.setParameter(user, EntityField.LOCKED, "false");
    }

}
