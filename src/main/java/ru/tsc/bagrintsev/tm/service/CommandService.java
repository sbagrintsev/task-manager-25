package ru.tsc.bagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.ICommandRepository;
import ru.tsc.bagrintsev.tm.api.sevice.ICommandService;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.bagrintsev.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;
import java.util.Optional;

public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;


    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAvailableCommands() {
        return commandRepository.getAvailableCommands();
    }

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByName(@Nullable final String name) throws AbstractException {
        check(EntityField.COMMAND_NAME, name);
        @NotNull final Optional<AbstractCommand> command = Optional.of(commandRepository.getCommandByName(name));
        return command.get();
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByShort(final @Nullable String shortName) throws AbstractException {
        check(EntityField.COMMAND_SHORT, shortName);
        @NotNull final Optional<AbstractCommand> command = Optional.of(commandRepository.getCommandByShort(shortName));
        return command.get();
    }

}
