package ru.tsc.bagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.sevice.ITaskService;
import ru.tsc.bagrintsev.tm.enumerated.Entity;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.Date;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final ITaskRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.NAME, name);
        return repository.create(userId, name);
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.NAME, name);
        check(EntityField.DESCRIPTION, description);
        return repository.create(userId, name, description);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.PROJECT_ID, projectId);
        return repository.findAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(index);
        check(EntityField.NAME, name);
        check(EntityField.DESCRIPTION, description);
        @NotNull final Task task = findOneByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @NotNull
    @Override
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.TASK_ID, id);
        check(EntityField.NAME, name);
        check(EntityField.DESCRIPTION, description);
        @NotNull final Task task = findOneById(userId, id);
        check(Entity.TASK, task);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @NotNull
    @Override
    public Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(index);
        check(Entity.STATUS, status);
        @NotNull final Task task = findOneByIndex(userId, index);
        task.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            task.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            task.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            task.setDateStarted(null);
            task.setDateFinished(null);
        }
        return task;
    }

    @NotNull
    @Override
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.TASK_ID, id);
        check(Entity.STATUS, status);
        @NotNull final Task task = findOneById(userId, id);
        task.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            task.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            task.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            task.setDateStarted(null);
            task.setDateFinished(null);
        }
        return task;
    }

}
