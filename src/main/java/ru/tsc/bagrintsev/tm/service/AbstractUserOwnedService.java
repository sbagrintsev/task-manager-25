package ru.tsc.bagrintsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.IUserOwnedRepository;
import ru.tsc.bagrintsev.tm.api.sevice.IUserOwnedService;
import ru.tsc.bagrintsev.tm.enumerated.Entity;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>> extends AbstractService<M, R> implements IUserOwnedService<M> {


    public AbstractUserOwnedService(@NotNull final R repository) {
        super(repository);
    }

    @NotNull
    @Override
    public M add(
            @Nullable final String userId,
            @Nullable final M record) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(Entity.ABSTRACT, record);
        return repository.add(userId, record);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) throws AbstractException {
        check(EntityField.USER_ID, userId);
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<M> comparator) throws AbstractException {
        check(EntityField.USER_ID, userId);
        if (comparator == null) {
            return findAll(userId);
        }
        return repository.findAll(userId, comparator);
    }

    @NotNull
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort) throws AbstractException {
        check(EntityField.USER_ID, userId);
        if (sort == null) {
            return findAll(userId);
        }
        return findAll(userId, (Comparator<M>) sort.getComparator());
    }

    @NotNull
    @Override
    public M findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(index);
        return repository.findOneByIndex(userId, index);
    }

    @NotNull
    @Override
    public M findOneById(
            @Nullable final String userId,
            @Nullable final String id) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.ID, id);
        return repository.findOneById(userId, id);
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.ID, id);
        return repository.existsById(userId, id);
    }

    @NotNull
    @Override
    public M remove(
            @Nullable final String userId,
            @Nullable final M record) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(Entity.ABSTRACT, record);
        return repository.remove(userId, record);
    }

    @NotNull
    @Override
    public M removeByIndex(
            @Nullable final String userId,
            @Nullable final Integer index) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(index);
        return repository.removeByIndex(userId, index);
    }

    @NotNull
    @Override
    public M removeById(
            @Nullable final String userId,
            @Nullable final String id) throws AbstractException {
        check(EntityField.USER_ID, userId);
        check(EntityField.ID, id);
        return repository.removeById(userId, id);
    }

    @Override
    public long totalCount(@Nullable final String userId) throws AbstractException {
        check(EntityField.USER_ID, userId);
        return repository.totalCount(userId);
    }

    @Override
    public void clear(@Nullable final String userId) throws AbstractException {
        check(EntityField.USER_ID, userId);
        repository.clear(userId);
    }

}
