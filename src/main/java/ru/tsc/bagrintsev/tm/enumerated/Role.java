package ru.tsc.bagrintsev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.entity.IncorrectRoleException;

@Getter
public enum Role {

    REGULAR("Regular user"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static Role toRole(@Nullable final String value) throws IncorrectRoleException {
        if (value == null || value.isEmpty()) return Role.REGULAR;
        for (Role role : Role.values()) {
            if (role.toString().equals(value)) {
                return role;
            }
        }
        throw new IncorrectRoleException();
    }

}
