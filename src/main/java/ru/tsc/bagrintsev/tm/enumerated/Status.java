package ru.tsc.bagrintsev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static Status toStatus(@Nullable final String value) throws IncorrectStatusException {
        if (value == null || value.isEmpty()) throw new IncorrectStatusException();
        for (Status status : Status.values()) {
            if (status.toString().equals(value)) {
                return status;
            }
        }
        throw new IncorrectStatusException();
    }

    @NotNull
    public static String toName(@Nullable final Status status) {
        if (status == null) return "";
        for (Status value : Status.values()) {
            if (value.equals(status)) {
                return status.getDisplayName();
            }
        }
        return "";
    }

}
