package ru.tsc.bagrintsev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.comparator.DateCreatedComparator;
import ru.tsc.bagrintsev.tm.comparator.DateStartedComparator;
import ru.tsc.bagrintsev.tm.comparator.NameComparator;
import ru.tsc.bagrintsev.tm.comparator.StatusComparator;

import java.util.Comparator;

@Getter
public enum Sort {

    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_CREATED("Sort by date created", DateCreatedComparator.INSTANCE),
    BY_STARTED("Sort by date started", DateStartedComparator.INSTANCE);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator<?> comparator;

    Sort(
            @NotNull final String displayName,
            @NotNull final Comparator<?> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return Sort.BY_CREATED;
        for (Sort sort : Sort.values()) {
            if (sort.toString().equals(value)) {
                return sort;
            }
        }
        return null;
    }

}
