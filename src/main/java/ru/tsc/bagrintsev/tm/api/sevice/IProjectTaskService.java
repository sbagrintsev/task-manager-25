package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

public interface IProjectTaskService extends Checkable {

    void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) throws AbstractException;

    void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) throws AbstractException;

    void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId) throws AbstractException;

}
