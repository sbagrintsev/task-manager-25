package ru.tsc.bagrintsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectParameterNameException;
import ru.tsc.bagrintsev.tm.model.User;

import java.security.GeneralSecurityException;

public interface IUserRepository extends IAbstractRepository<User> {

    @NotNull
    User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final byte[] salt) throws GeneralSecurityException;

    @NotNull
    User setParameter(
            @NotNull final User user,
            @NotNull final EntityField paramName,
            @NotNull final String paramValue) throws IncorrectParameterNameException;

    @NotNull
    User setRole(
            @NotNull final User user,
            @NotNull final Role role);

    void setUserPassword(
            @NotNull final User user,
            @NotNull final String password,
            @NotNull byte[] salt) throws GeneralSecurityException;

    @NotNull
    User findByLogin(@NotNull final String login) throws UserNotFoundException;

    @NotNull
    User findByEmail(@NotNull final String email) throws UserNotFoundException;

    @NotNull
    User removeByLogin(@NotNull final String login) throws UserNotFoundException;

}
