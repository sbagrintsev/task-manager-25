package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.Entity;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.*;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.bagrintsev.tm.exception.system.CommandNotSupportedException;
import ru.tsc.bagrintsev.tm.exception.user.LoginIsIncorrectException;
import ru.tsc.bagrintsev.tm.exception.user.PasswordIsIncorrectException;

public interface Checkable {

    default void check(
            @NotNull final EntityField field,
            @Nullable final String attribute) throws AbstractException {
        if (attribute == null || attribute.isEmpty()) {
            switch (field) {
                case NAME:
                    throw new NameIsEmptyException();
                case DESCRIPTION:
                    throw new DescriptionIsEmptyException();
                case ID:
                    throw new IdIsEmptyException();
                case PROJECT_ID:
                case TASK_ID:
                case USER_ID:
                    throw new IdIsEmptyException(field.getDisplayName());
                case EMAIL:
                    throw new EmailIsEmptyException();
                case LOGIN:
                    throw new LoginIsIncorrectException();
                case PASSWORD:
                    throw new PasswordIsIncorrectException();
                case COMMAND_NAME:
                    throw new CommandNotSupportedException(attribute);
                case COMMAND_SHORT:
                    throw new ArgumentNotSupportedException(attribute);
            }
        }
    }

    default void check(
            @NotNull final Entity entity,
            @Nullable final Object instance) throws AbstractException {
        if (instance == null) {
            switch (entity) {
                case TASK:
                    throw new TaskNotFoundException();
                case PROJECT:
                    throw new ProjectNotFoundException();
                case USER:
                    throw new UserNotFoundException();
                case ABSTRACT:
                    throw new ModelNotFoundException();
                case ROLE:
                    throw new IncorrectRoleException();
                case STATUS:
                    throw new IncorrectStatusException();
            }
        }
    }

}
