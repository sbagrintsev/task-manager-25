package ru.tsc.bagrintsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(
            @NotNull final String userId,
            @NotNull final String name);

    @NotNull
    Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description);

}
