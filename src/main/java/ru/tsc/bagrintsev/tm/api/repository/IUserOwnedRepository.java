package ru.tsc.bagrintsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IAbstractRepository<M> {

    @NotNull
    M add(
            @NotNull final String userId,
            @NotNull final M record) throws AbstractException;

    @NotNull
    List<M> findAll(@NotNull final String userId) throws AbstractException;

    @NotNull
    List<M> findAll(
            @NotNull final String userId,
            @NotNull final Comparator<M> comparator) throws AbstractException;

    @NotNull
    M findOneByIndex(
            @NotNull final String userId,
            @NotNull final Integer index) throws AbstractException;

    @NotNull
    M findOneById(
            @NotNull final String userId,
            @NotNull final String id) throws AbstractException;

    boolean existsById(
            @NotNull final String userId,
            @NotNull final String id) throws AbstractException;

    @NotNull
    M remove(
            @NotNull final String userId,
            @NotNull final M record) throws AbstractException;

    @NotNull
    M removeByIndex(
            @NotNull final String userId,
            @NotNull final Integer index) throws AbstractException;

    @NotNull
    M removeById(
            @NotNull final String userId,
            @NotNull final String id) throws AbstractException;

    long totalCount(@NotNull final String userId) throws AbstractException;

    void clear(@NotNull final String userId) throws AbstractException;

}
