package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(
            @Nullable final String userId,
            @Nullable final String name) throws AbstractException;

    @NotNull
    Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description) throws AbstractException;

    @NotNull
    List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId) throws AbstractException;

    @NotNull
    Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description) throws AbstractException;

    @NotNull
    Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description) throws AbstractException;

    @NotNull
    Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status) throws AbstractException;

    @NotNull
    Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status) throws AbstractException;

}
