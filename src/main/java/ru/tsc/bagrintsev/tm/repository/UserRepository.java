package ru.tsc.bagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.repository.IUserRepository;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectParameterNameException;
import ru.tsc.bagrintsev.tm.model.User;

import java.security.GeneralSecurityException;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final byte[] salt) throws GeneralSecurityException {
        @NotNull final User user = new User();
        user.setLogin(login);
        setUserPassword(user, password, salt);
        return add(user);
    }

    @NotNull
    @Override
    public User setParameter(
            @NotNull final User user,
            @NotNull final EntityField paramName,
            @NotNull final String paramValue) throws IncorrectParameterNameException {
        switch (paramName) {
            case EMAIL:
                user.setEmail(paramValue);
                break;
            case FIRST_NAME:
                user.setFirstName(paramValue);
                break;
            case MIDDLE_NAME:
                user.setMiddleName(paramValue);
                break;
            case LAST_NAME:
                user.setLastName(paramValue);
                break;
            case LOCKED:
                user.setLocked(paramValue.equals("true"));
                break;
            default:
                throw new IncorrectParameterNameException(paramName, "User");
        }
        return user;
    }

    @NotNull
    @Override
    public User setRole(
            @NotNull final User user,
            @NotNull final Role role) {
        user.setRole(role);
        return user;
    }

    @Override
    public void setUserPassword(
            @NotNull final User user,
            @NotNull final String password,
            @NotNull final byte[] salt){
        user.setPasswordSalt(salt);
        user.setPasswordHash(password);
    }

    @NotNull
    @Override
    public User findOneById(@NotNull final String id) throws AbstractException {
        return records.stream()
                .filter(record -> id.equals(record.getId()))
                .findFirst()
                .orElseThrow(() -> new UserNotFoundException("Id", id));
    }

    @NotNull
    @Override
    public User findByLogin(@NotNull final String login) throws UserNotFoundException {
        return records.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst()
                .orElseThrow(() -> new UserNotFoundException("Login", login));
    }

    @NotNull
    @Override
    public User findByEmail(@NotNull final String email) throws UserNotFoundException {
        return records.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst()
                .orElseThrow(() -> new UserNotFoundException("Email", email));
    }

    @NotNull
    @Override
    public User removeByLogin(@NotNull final String login) throws UserNotFoundException {
        @NotNull final User user = findByLogin(login);
        return remove(user);
    }

}
