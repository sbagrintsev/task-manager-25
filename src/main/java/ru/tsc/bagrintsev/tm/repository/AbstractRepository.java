package ru.tsc.bagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.IAbstractRepository;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IAbstractRepository<M> {

    @NotNull
    protected final List<M> records = new ArrayList<>();

    @NotNull
    @Override
    public M add(@NotNull final M record) {
        records.add(record);
        return record;
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return records;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        return records.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        return records.get(index);
    }

    @NotNull
    @Override
    public M findOneById(@NotNull final String id) throws AbstractException {
        return records.stream()
                .filter(record -> id.equals(record.getId()))
                .findFirst()
                .orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public boolean existsById(@NotNull final String id) throws AbstractException {
        findOneById(id);
        return true;
    }

    @NotNull
    @Override
    public M remove(@NotNull final M record) {
        records.remove(record);
        return record;
    }

    @NotNull
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        final M record = findOneByIndex(index);
        return remove(record);
    }

    @NotNull
    @Override
    public M removeById(@NotNull final String id) throws AbstractException {
        final M record = findOneById(id);
        return remove(record);
    }

    @Override
    public int totalCount() {
        return records.size();
    }

    @Override
    public void clear() {
        records.clear();
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collection) {
        records.removeAll(collection);
    }

}
