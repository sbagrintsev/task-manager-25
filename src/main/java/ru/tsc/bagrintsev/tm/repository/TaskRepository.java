package ru.tsc.bagrintsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;


public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name) {
        @NotNull final Task task = new Task();
        task.setName(name);
        return add(userId, task);
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(userId, task);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId) {
        return records.stream()
                .filter(record -> projectId.equals(record.getProjectId()) && userId.equals(record.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public void setProjectId(
            @NotNull final String userId,
            @NotNull final String taskId,
            @NotNull final String projectId) throws AbstractException {
        @NotNull final Task task = findOneById(userId, taskId);
        task.setProjectId(projectId);
    }

}
