package ru.tsc.bagrintsev.tm.exception.field;


import org.jetbrains.annotations.Nullable;

public final class IdIsEmptyException extends AbstractFieldException {

    public IdIsEmptyException() {
        super("Error! Id is empty...");
    }

    public IdIsEmptyException(@Nullable final String entityType) {
        super(String.format("Error! %s is empty...", entityType.substring(0, 1).toUpperCase() + entityType.substring(1)));
    }

}
