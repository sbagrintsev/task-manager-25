package ru.tsc.bagrintsev.tm.exception.entity;

import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

public class AbstractEntityException extends AbstractException {

    public AbstractEntityException() {
        super();
    }

    public AbstractEntityException(@Nullable final String message) {
        super(message);
    }

    public AbstractEntityException(
            @Nullable final String message,
            @Nullable final Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityException(@Nullable final Throwable cause) {
        super(cause);
    }

    protected AbstractEntityException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
