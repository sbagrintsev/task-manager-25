package ru.tsc.bagrintsev.tm.exception.user;

import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

public class AbstractUserException extends AbstractException {

    public AbstractUserException() {
        super();
    }

    public AbstractUserException(@Nullable final String message) {
        super(message);
    }

    public AbstractUserException(
            @Nullable final String message,
            @Nullable final Throwable cause) {
        super(message, cause);
    }

    public AbstractUserException(@Nullable final Throwable cause) {
        super(cause);
    }

    protected AbstractUserException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
